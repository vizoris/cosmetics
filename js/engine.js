$(function() {

$('.product-slider').slick({
    infinite: true,
    slidesToShow: 4,
    slidesToScroll: 1,
    // autoplay: true,
    // autoplaySpeed: 4000,
    pauseOnHover: false,
    responsive: [
    {
      breakpoint: 1200,
      settings: {
        slidesToShow: 3,
      }
    },
    {
      breakpoint: 767,
      settings: {
        slidesToShow: 1,
      }
    },
  ]
});

$('.logo-slider').slick({
    infinite: true,
    slidesToShow: 6,
    slidesToScroll: 1,
    // autoplay: true,
    // autoplaySpeed: 4000,
    pauseOnHover: false,
    responsive: [
    {
      breakpoint: 1200,
      settings: {
        slidesToShow: 3,
      }
    },
    {
    breakpoint: 767,
      settings: {
        slidesToShow: 1,
      }
    },
  ]
});

$('.reviews-slider').slick({
    infinite: true,
    slidesToShow: 2,
    slidesToScroll: 1,
    // autoplay: true,
    // autoplaySpeed: 4000,
    pauseOnHover: false,
    responsive: [
   {
    breakpoint: 767,
      settings: {
        slidesToShow: 1,
      }
    },
  ]
});


// BEGIN of script for header submenu
    $(".navbar-toggle").on("click", function () {
        $(this).toggleClass("active");
    });




// Выпадающее меню
if($(window).width() > 768){

	$('.dropdown').hover(function() {
		$(this).children('a').stop(true,true).toggleClass('active');
		$(this).children(".dropdown-menu").stop(true,true).fadeToggle();
	})

	$('.dropdown-menu > li').hover(function() {
	    $(this).children('ul').toggleClass('active');
	})

	$('.dropdown-level2').hover(function() {
	    $(this).toggleClass('hover');
	})

}


if($(window).width() < 768){

	$('.navbar-toggle').click(function() {
		$('.navbar-collapse').toggleClass('in');
		$('.navbar-nav').toggleClass('active');
	})

  $('.mobile-toggler').click(function() {
    $(this).toggleClass('active');
    $(this).next("ul").stop(true,true).fadeToggle();
  })




}



// Таймеры обратного счета
$('#clock1, #clock2').countdown('2020/10/10', function(event) {
  var $this = $(this).html(event.strftime(''
	+ ' <div class="clock-item"><span class="clock-item__value">%H</span> <span class="clock-item__title"> часов </span></div>'
	+ ' <div class="clock-item"><span class="clock-item__value">%M</span> <span class="clock-item__title"> минут </span></div>'
	+ ' <div class="clock-item"><span class="clock-item__value">%S</span> <span class="clock-item__title"> секунд </span></div>'

	));
});


// Клик на сердечко товара (лайк)
$('.product-like').click(function() {
	$(this).toggleClass('active')
});

// Табы
$('.tabs-nav a').click(function() {
    var _id = $(this).attr('href');
    var _targetElement = $(this).parents('.tabs-wrapper').find('#' + _id);
    $('.tabs-nav a').removeClass('active');
    $(this).addClass('active');
    $('.tabs-item').removeClass('active');
    _targetElement.addClass('active');
    return false;

});


$('.tabs-mobile__btn').click(function() {
    var _targetElementParent = $(this).parents('.tabs-item');
    _targetElementParent.toggleClass('active-m');

    _targetElementParent.find('.tabs-item__body').slideToggle(600);
    $('.tabs-mobile__btn').parent('.tabs-item').removeClass('active');
    return false;
});


// Сайдбар на планшетах и мобильных
$('.sidebar-btn').click(function() {
	$(this).toggleClass('hidden');
	$('.sidebar').toggleClass('active');
})
$('.sidebar-close').click(function() {
	$('.sidebar').toggleClass('active');
	$('.sidebar-btn').toggleClass('hidden');
})

})